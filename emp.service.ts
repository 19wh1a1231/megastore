import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  public userLoggedIn : boolean;  //user-define
  constructor(private httpClient: HttpClient) {
    this.userLoggedIn = false;//initially set to false
   }
   getUserLoggedIn(){
     return this.userLoggedIn; // invoked whenever we need this value
   }
   setUserLoggedIn(){
     this.userLoggedIn = true; // invoked whenever we logged in successfully...
   }
   setUserLoggedOut(){
    this.userLoggedIn = false; // invoked whenever we logged out
  }
   showAllEmployees(){
     return this.httpClient.get('http://localhost:3000/fetch');
   }

  registerEmp(employee: any){
    return this.httpClient.post('http://localhost:3000/register/', employee);
  }

  getEmployeeByEmailAndPassword(loginForm: any){
    return this.httpClient.get('http://localhost:3000/login/' + loginForm.email+"/"+loginForm.password).toPromise();
  }

 
}



